/* Without Promise.try */

function doAsyncStuff(value) {
	return db.getSomeItem(value)
		.then((item) => {
			return item.name;
		});
}