/* With Promise.try */

function doAsyncStuff(value) {
	return Promise.try(() => {
		return db.getSomeItem(value);
	}).then((item) => {
		return item.name;
	});
}